package store

type Car struct {
	ID        string
	Brand     string
	Model     string
	Price     float64
	SellPrice float64
	Color     string
	Available bool
}

func NewCar(id string, brand string, model string, price float64, color string) *Car {
	return &Car{
		ID:        id,
		Brand:     brand,
		Model:     model,
		Price:     price,
		SellPrice: price,
		Color:     color,
		Available: true,
	}
}

func (c *Car) sell(sellPrice float64) {
	c.Available = false
	c.SellPrice = sellPrice
}
