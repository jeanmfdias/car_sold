package store

import "testing"

func TestNewCar(t *testing.T) {
	id := "1"
	brand := "Jeep"
	model := "Renegade"
	price := 79000.00
	color := "GRAY"

	car := NewCar(id, brand, model, price, color)
	if car == nil {
		t.Fatalf("got %q, wanted %q", "new car", "nil")
	}
	if car.Model != model {
		t.Fatalf("got %q, wanted %q", car.Model, model)
	}
}

func TestSellCar(t *testing.T) {
	id := "1"
	brand := "Jeep"
	model := "Renegade"
	price := 79000.00
	color := "GRAY"

	car := NewCar(id, brand, model, price, color)

	sellPrice := 80000.00
	car.sell(sellPrice)

	if car.SellPrice != sellPrice {
		t.Fatalf("got %f, wanted %f", car.SellPrice, sellPrice)
	}
}
